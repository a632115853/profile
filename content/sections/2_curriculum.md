---
title: Currículum
date: 2022-01-25T14:00:00.000Z
authorbox: false
sidebar: false
menu:
  main:
    name: Currículum
    weight: 3
---

## Sobre mi

Hola, mi nombre es <b>Javier Sánchez</b>, soy una persona autodidacta con un <b>largo recorrido en el mundo de la informática</b> por <b>pasión</b> y <b>hobby</b>. Me encantaría continuar desarrollando mi carrera profesional en las áreas de <b>Programación</b>, <b>Administración de sistemas Linux</b>, <b>DevOps / Operaciones</b>, <b>Ciberseguridad</b> y/o <b>Redes Juniper</b>.

Además de una sólida formación, mis años de estudio autodidacta me han permitido desarrollar una <b>gran capacidad analítica</b>, de aprendizaje y curiosidad por cómo funciona la tecnología y el mundo, lo que me permite adaptarme, entender y diseñar nuevos entornos</b> con la <b>sencillez, alta disponibilidad, tolerancia a fallos y seguridad</b> en mente.

<h4>Áreas, experiencia y certificaciones en:</h4>

* Programación (Backend, automatización, sistema)
* Administración de sistemas Linux (Nivel de conocimientos +- LPI-2)
* Administración de red (Cisco y Juniper)
* CiberSeguridad

<h4>Mi nivel técnico y perfil autodidacta</h4>

Creo poseer un nivel técnico bastante alto, habiendo adquirido la mayor parte de dichas habilidades de forma autodidacta leyendo libros y otro tipo de documentación.

Algunos ejemplos:
* Comandos strace, ptrace, ltrace para investigación y resolución de problemas.
* Conocimientos sobre el <b>kernel de linux</b> y <b>módulos</b>.
* <b>Programación a bajo nivel</b>.
* <b>Ciberseguridad en entornos web</b>, sistema y servicios.
* . . .

<h4>Que busco</h4>

Actualmente me encuentro en búsqueda activa de empleo con la finalidad de crecer profesionalmente desarrollando y perfeccionando mis habilidades en las áreas de:
* CiberSeguridad
* DevOps / DevSecOps / Operaciones
* Ciberseguridad
* Clowd
* Redes Juniper

<h4>Carta de presentación + Curriculum vitae completo</h4>

<b>Acceder al CV en ventana nueva:</b> [https://t.ly/MuO0](https://t.ly/MuO0)

<!-- ##################### Curriculum Vitae ##################### -->
<!-- ############################################################ -->
<iframe  frameborder="0" scrolling="yes" seamless="seamless" style="display:block; width:100%; height:100vh;" src="https://t.ly/MuO0"></iframe>
<!-- ############################################################ -->